package net.trincom.mscassandraconnector;

import net.trincom.mscassandraconnector.cassandra.CassandraTestExecutionListener;
import net.trincom.mscassandraconnector.cassandra.TestCassandraConfig;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;


@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("testing")
@ContextConfiguration(
		classes = { TestCassandraConfig.class },
		initializers = ConfigFileApplicationContextInitializer.class)
@TestExecutionListeners({
		CassandraTestExecutionListener.class,
		DependencyInjectionTestExecutionListener.class })
public class MsCassandraConnectorApplicationTests {

	@Test
	void contextLoads() {

	}
}
