package net.trincom.mscassandraconnector.controller;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import net.trincom.mscassandraconnector.model.DataContainer;
import net.trincom.mscassandraconnector.model.ProcessNotification;
import net.trincom.mscassandraconnector.service.ProcessNotificationService;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.header.Headers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;


@RestController
@RequestMapping("/notification")
@RequiredArgsConstructor
public class ProcessNotificationController {

    private static final Logger logger = LoggerFactory.getLogger(ProcessNotificationController.class);
    private final ProcessNotificationService service;

    @GetMapping(path = "/")
    public List<ProcessNotification> fetchAll() {
        return service.findAll();
    }

    @GetMapping(path = "/{dataId}")
    public ResponseEntity<ProcessNotification> fetchNotificationById(@PathVariable("dataId") String dataId) {
        Optional<ProcessNotification> fetchData = service.findByDataId(dataId);
        return fetchData.map(processNotification -> new ResponseEntity<>(processNotification, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(ProcessNotification.empty(), HttpStatus.OK));
    }

    @KafkaListener(topics = "${tpd.topic-name}", clientIdPrefix = "json", containerFactory = "kafkaListenerStringContainerFactory")
    public void listenToNotificationStream(ConsumerRecord<String, String> cr, @Payload String payload) throws JsonProcessingException {
        DataContainer dataContainer = new ObjectMapper().readValue(payload, DataContainer.class);
        System.out.println("Received: " + dataContainer.getId());
        //logger.info("Logger 1 [JSON] received key {}: Type [{}] | Payload: {} | Record: {}", cr.key(), typeIdHeader(cr.headers()), payload, cr.toString());
        ProcessNotification notification = service.create(new ProcessNotification(dataContainer));
        System.out.println("Persisted: " + notification.getId());
    }

    private static String typeIdHeader(Headers headers) {
        return StreamSupport.stream(headers.spliterator(), false)
                .filter(header -> header.key().equals("__TypeId__"))
                .findFirst().map(header -> new String(header.value())).orElse("N/A");
    }
}
