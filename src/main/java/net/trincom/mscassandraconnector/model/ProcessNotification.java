package net.trincom.mscassandraconnector.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;


@Data
@Table("process_notification")
@AllArgsConstructor
@NoArgsConstructor
public class ProcessNotification implements Serializable {

    private static final long serialVersionUID = 1L;

    @PrimaryKeyColumn(name = "id", ordinal = 0, type = PrimaryKeyType.PARTITIONED)
    @Column("id")
    @GeneratedValue
    private String id;

    @PrimaryKeyColumn(name = "dataId", ordinal = 1, type = PrimaryKeyType.PARTITIONED)
    @Column("data_id")
    private String dataId;

    @Column("created_time")
    private Date createdTime  = new Date();

    public ProcessNotification(DataContainer dataContainer) {
        this.id = UUID.randomUUID().toString(); // XXX this should be handled over a sequence generator - for some reason javax doesn't work
        this.dataId = dataContainer.getId();
        this.createdTime = new Date();
    }

    public static ProcessNotification empty() {
        return new ProcessNotification("0", "0", new Date());
    }
}