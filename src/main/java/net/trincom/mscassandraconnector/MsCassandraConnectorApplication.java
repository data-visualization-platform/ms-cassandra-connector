package net.trincom.mscassandraconnector;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.cassandra.repository.config.EnableCassandraRepositories;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@EnableAutoConfiguration
@SpringBootApplication(scanBasePackages = {"net.trincom"})
@EnableCassandraRepositories(basePackages = {"net.trincom"})
public class MsCassandraConnectorApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsCassandraConnectorApplication.class, args);
	}

	@GetMapping("/")
	public String base() {
		return "Microservice - Cassandra Connector up and running!";
	}
}
