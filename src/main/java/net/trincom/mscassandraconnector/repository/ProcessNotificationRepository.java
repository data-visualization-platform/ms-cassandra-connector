package net.trincom.mscassandraconnector.repository;

import net.trincom.mscassandraconnector.model.ProcessNotification;
import org.springframework.data.cassandra.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;


@Repository
public interface ProcessNotificationRepository extends CrudRepository<ProcessNotification, Serializable> {

    @Query(allowFiltering = true, value = "SELECT * from process_notification")
    List<ProcessNotification> findAll();

    @Query(allowFiltering = true, value = "SELECT * from process_notification where dataid = ?0")
    Optional<ProcessNotification> findByDataId(String dataId);
}