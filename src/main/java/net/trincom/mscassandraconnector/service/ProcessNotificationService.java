package net.trincom.mscassandraconnector.service;

import lombok.RequiredArgsConstructor;
import net.trincom.mscassandraconnector.model.ProcessNotification;
import net.trincom.mscassandraconnector.repository.ProcessNotificationRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
@RequiredArgsConstructor
public class ProcessNotificationService {

    private final ProcessNotificationRepository repository;


    public Optional<ProcessNotification> findByDataId(String dataId) {
        return repository.findByDataId(dataId);
    }
    public List<ProcessNotification> findAll() { return repository.findAll(); }
    public ProcessNotification create(ProcessNotification notification) { return repository.save(notification); }
}
