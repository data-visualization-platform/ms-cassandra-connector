# BUILD STAGE
FROM gradle:jdk11 as compile
USER root
RUN mkdir -p /app/src
COPY . /app/src
WORKDIR /app/src
RUN chown -R gradle /app/src
USER gradle
RUN gradle build --no-daemon

# RUNTIME
FROM openjdk:11-jre-slim as runtime
ARG BROKERS_IP_ADDRESSES
ARG KAFKA_ADVERTISED_HOST_NAME
ARG CASSANDRA_HOST
ENV BROKERS_IP_ADDRESSES=$BROKERS_IP_ADDRESSES
ENV KAFKA_ADVERTISED_HOST_NAME=$KAFKA_ADVERTISED_HOST_NAME
ENV CASSANDRA_HOST=$CASSANDRA_HOST

COPY --from=compile /app/src/build/libs/*.jar /app/app.jar
EXPOSE 8085
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "/app/app.jar"]